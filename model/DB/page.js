const mongoose = require('./mongoose');
const schema = require("./Schema/page").schema;

module.exports = mongoose.model("Page", schema);