const mongoose = require('../mongoose');
const schema = new mongoose.Schema({
    title: {type: String, required: true, unique: true},
    text: {type: String, required: true, default: ""},
});
module.exports.schema = schema;