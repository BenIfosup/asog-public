const mongoose = require('../mongoose');
const schema = new mongoose.Schema({
    pseudo: {type: String, required: true},
    mdp: {type: String, required: true}
});
module.exports.schema = schema;