const mongoose = require('./mongoose');
const schema = require("./Schema/article").schema;
const User = require('./user');

schema.pre('save', function(){
    return new Promise((resolve, reject) => {
        User.findById(this.author).countDocuments().then(val => {
            if(val === 1){
                resolve();
            }
            else{
                reject(new Error(`L'utilisateur avec l'ID ${this.auteur} n'existe pas`));
            }
        }, err =>{
            console.log(err);
            reject(err);
        })
    })
});

const model = mongoose.model("Article", schema);
module.exports = model;