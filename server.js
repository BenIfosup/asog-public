const express = require('express');
const app = express();
const morgan = require('morgan');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const multer = require("multer");
const storage = multer.memoryStorage();
const path = require('path');
const helmet = require('helmet');

const article = require('./controleur/article');
const user = require('./controleur/user');
const page = require('./controleur/page');
const upload = require('./controleur/upload');
const graphQL = require('express-graphql');
const rss = require('./controleur/rss');
const image = require('./controleur/image');
const contact = require('./controleur/contact');

app.use(helmet());
app.use(morgan('dev'));
app.use(cookieParser());
app.use(session({
    secret: "42 is the key",
    path:"/",
    resave: false,
    saveUninitialized: false
}));

app.use(multer({ storage }).single('file'));
app.use(express.json());

app.use("/static", express.static(path.join(__dirname, 'static')));
app.use("/public", express.static(path.join(__dirname, 'public')));
app.get("/upload/images/:filename/:width", image.image);
app.use("/upload", express.static(path.join(__dirname, 'upload')));

app.post('/user',graphQL({schema: user, graphiql: false}));
app.post('/article',graphQL( (req, res ) => {
    let width = '';
    if(req.body && req.body.variables && req.body.variables.width){
        width = req.body.variables.width;
    }
    return {
        schema: article,
        context: {request: req, response: res, width},
        graphiql: false
    }
}));
app.post('/page', graphQL({schema: page, graphiql: false}));

app.get("/rss", rss.generateRSS);
app.post("/uploadImage", upload.uploadImage);
app.get("/admin", (req, res) =>  res.sendFile(__dirname+"/index.html"));
app.get("/robots.txt", (req, res) => res.sendFile(__dirname+"/robots.txt"));
app.post("/contact", contact.handleRequestContact);

app.get("/", (req, res) =>  res.sendFile(__dirname+"/index.html"));

app.use('*', (req, res) => res.sendFile(__dirname+"/index.html"));

app.listen(8080);