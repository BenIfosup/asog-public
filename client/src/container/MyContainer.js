import React from 'react';
import {Container} from "semantic-ui-react";
import MyHeader from '../component/admin/header';
import PanelAdmin from '../component/panelAdmin';
import {connect} from "react-redux";
import axios from 'axios';

class MyContainer extends React.PureComponent{

    deconnexion(){
        const options = {
            url: '/user',
            method: 'POST',
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
               query: `query{
                            deconnexion
                      }`,
               variable: null
            })
        };
        axios(options).then(val => {
            if(val.data.data.deconnexion){
                this.props.disableConnexion();
            }
        });
    }

    render(){
        return (
            <Container>
                <MyHeader deconnexion={() => this.deconnexion()}/>
                <PanelAdmin {...this.props} right={this.props.right}/>
            </Container>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        disableConnexion: () => {
            dispatch({type: "disableConnexion"});
        }
    }
};

export default connect(() => {return {}}, mapDispatchToProps)(MyContainer);