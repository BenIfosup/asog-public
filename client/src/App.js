import React, { Component, Suspense} from 'react';
import './style/styles.scss';
import {Route, Switch} from "react-router-dom";
import axios from "axios";
import {connect} from "react-redux";

const AdminRoute = React.lazy(() => import('./route/adminRoute'));
const PublicRoute = React.lazy(() => import('./route/publicRoute'));

class App extends Component{

    componentDidMount() {
        const options = {
            url: "/user",
            method: "POST",
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query: `query{
                    isAlreadyConnected{
                        id
                    }
                }`,
                variables: null
            })
        };
        axios(options).then(val => {
            if(val.data.data.isAlreadyConnected && val.data.data.isAlreadyConnected.id){
                this.props.enableConnexion(val.data.data.isAlreadyConnected.id);
            }
        })
    }

    render(){
        return (
            <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Route path={'/admin'} component={AdminRoute}/>
                        <Route path={'/'} component={PublicRoute} />
                    </Switch>
            </Suspense>
        )
    }
}

const mapStateToProps = (state) =>{
    return {
        connected : state.connexion.connected
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        enableConnexion: (id) => {
            dispatch({type: "enableConnexion", payload:{id}});
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(App);