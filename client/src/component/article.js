import React from 'react';
import {Image, Header, Segment, Dimmer, Loader} from "semantic-ui-react";
import Divider from "semantic-ui-react/dist/commonjs/elements/Divider";
import moment from 'moment';
import axios from 'axios';
import HOC from '../container/hoc';
import MetaTags from "react-meta-tags"
import Utils from '../utils/utils';

class ArticleContent extends React.PureComponent{

    constructor(props){
        super(props);
        this.state = {
            article: props.article,
            author: props.author
        }
    }

    render(){
        return(
            <article className={"article"}>
                <MetaTags>
                    <title>{this.state.article.title}</title>
                    <meta name="description" content={this.state.article.subtitle} />
                    <meta name="title" content={this.state.article.title} />
                    <meta name="author" content={this.state.author.pseudo}/>
                </MetaTags>
                <Image className='cover' src={this.state.article.header+'/'+Utils.getGoodImageSize()} alt={"Image de couverture de l'article"}/>
                <Header className="title" as={"h1"} dividing={true} content={this.state.article.title} />
                <Header as={"h2"} className="subtitle" content={this.state.article.subtitle} />
                <HOC>
                    <div dangerouslySetInnerHTML={{__html: this.state.article.text}}/>
                </HOC>
                <Divider/>
                <div className="info_article flexbox_horizontale flex_space_between">
                    <p className="author">
                        Auteur: {this.state.author.pseudo}
                    </p>
                    <p className="date_article">
                        {moment(this.state.article.date).format("DD/MM/YYYY")}
                    </p>
                </div>
            </article>
        );
    }
};

class Article extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            article : null,
            author: null
        }
    }

    componentDidMount() {
        const title = decodeURIComponent(this.props.args.title).replace(/_/g, " ");
        const options = {
            method: 'POST',
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query: `query{
                          articleWithAuthorByTitle(title: "${encodeURI(title)}"){
                            article{
                              header,
                              title,
                              subtitle,
                              text,
                              date
                            }
                            author{
                              pseudo
                            }
                          }
                        }`,
                variables: {
                    width: "200"
                }
            }),
            url: "/article"
        };

        axios(options).then(val => {
            let article = val.data.data.articleWithAuthorByTitle.article;
            article.text = Utils.imageURLGoodDimension(article.text);
            this.setState({
                article: article,
                author: val.data.data.articleWithAuthorByTitle.author
            });
        })
    }

    render(){
        return(
            <article>
                {
                    this.state.article ?
                        (
                            <ArticleContent {...this.state} />
                        )
                        :
                        (
                            <Segment className={"myLoader"}>
                                <Dimmer active>
                                    <Loader>Chargement</Loader>
                                </Dimmer>
                            </Segment>
                        )
                }
            </article>
        );
    }
}

export default Article;