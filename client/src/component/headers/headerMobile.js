import React from 'react';
import {Sidebar, Segment, Menu, Input, Icon, Button} from "semantic-ui-react";
import {Link, Redirect} from "react-router-dom";
import Logo from "../Logo/Logo";
import HOC from '../../container/hoc';

class headerMobile extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            searchField : "",
            sidebarVisible: false
        }
    }

    componentDidMount() {
        this.setState({
            redirect: false,
            searchField: "",
            sidebarVisible: false
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.redirect){
            this.setState({redirect: false, sidebarVisible: false});
        }

    }

    handleSidebarHide(){
        this.setState({sidebarVisible: false});
    }


    render(){
        return(
            <HOC>
                {
                    this.state.redirect ?
                        (<Redirect to={"/generalSearch/"+this.state.searchField}/>) : null
                }
                <Sidebar.Pushable as={Segment}>
                    <Sidebar
                        as={Menu}
                        animation='overlay'
                        onHide={() => this.handleSidebarHide()}
                        direction={"right"}
                        vertical
                        visible={this.state.sidebarVisible}
                    >
                        <Menu.Item>
                            <Link to={"/search/News"} onClick={() => this.handleSidebarHide()} className={"black"}><h2>News</h2></Link>
                        </Menu.Item>
                        <Menu.Item >
                            <Link to={"/search/Dossier"} onClick={() => this.handleSidebarHide()} className={"black"}><h2>Dossier</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/search/Test"} onClick={() => this.handleSidebarHide()} className={"black"}><h2>Test</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/search/Divers"} onClick={() => this.handleSidebarHide()} className={"black"}><h2>Divers</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/page/A_propos"} onClick={() => this.handleSidebarHide()} className={"black"}><h2>A propos</h2></Link>
                        </Menu.Item>
                    </Sidebar>
                    <Sidebar.Pusher className={"wrapper"}>
                        <header className={"flexbox_horizontale header_mobile"}>
                            <Link to={"/"} className={"logoLink"}>
                                <div><Logo/></div>
                            </Link>
                            <div className={"flexbox_horizontale flex_center_center mini_menu"}>
                                <div className="ui icon input search_field">
                                    <Input action size={'mini'} style={{flexGrow: 1}}>
                                        <input aria-label="recherche" onKeyUp={ e =>{
                                            if(e.key === "Enter" && e.target.value !== ""){
                                                this.setState({
                                                    redirect: true,
                                                    searchField: e.target.value
                                                });
                                            }
                                        }}/>
                                        <Button icon aria-label={"Recherche"} onClick={() => {
                                            if(this.state.searchField !== ""){
                                                this.setState({redirect: true});
                                            }
                                        }}>
                                            <Icon size={"large"} name={"search"}/>
                                        </Button>
                                    </Input>
                                </div>
                                <div className="right item">
                                    <Button
                                        icon
                                        aria-label={"Apparaître ou disparaître menu"}
                                        onClick={() => this.setState({sidebarVisible: true})}
                                    >
                                        <Icon size={"large"} name="bars"/>
                                    </Button>
                                </div>
                            </div>
                        </header>
                        {this.props.children}
                    </Sidebar.Pusher>
                </Sidebar.Pushable>
            </HOC>
        );
    }
}

export default headerMobile;
