import React from 'react';
import {Link, Redirect} from "react-router-dom";
import Logo from "../Logo/Logo";
import {Input, Menu} from "semantic-ui-react";

class HeaderDesktop extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            searchField : "",
            ...props
        }
    }

    componentDidMount() {
        this.setState({
            redirect: false,
            searchField: ""
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.redirect){
            this.setState({redirect: false});
        }
    }

    render(){
        return(
            <header className="header">
                {
                    this.state.redirect ?
                        (<Redirect to={"/generalSearch/"+this.state.searchField}/>) : null
                }
                <nav className="flexbox_horizontale">
                    <Link to="/" className="logoLink">
                        <div>
                            <Logo/>
                        </div>
                    </Link>
                    <Menu widths={6} color='black' inverted={true} secondary={true}>
                        <Menu.Item>
                            <Link to={"/search/News"} className="dark_hover"><h2>News</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/search/Dossier"} className="dark_hover"><h2>Dossier</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/search/Test"} className="dark_hover"><h2>Test</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/search/Divers"} className="dark_hover"><h2>Divers</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Link to={"/page/A_propos"} className="dark_hover"><h2>A propos</h2></Link>
                        </Menu.Item>
                        <Menu.Item>
                            <Input icon="search" aria-label="recherche" onKeyUp={ e => {
                                if(e.key === "Enter"){
                                    this.setState({
                                        redirect: true,
                                        searchField: e.target.value
                                    });
                                }
                            }}/>
                        </Menu.Item>
                    </Menu>
                </nav>
            </header>
        );
    }
}

export default HeaderDesktop;