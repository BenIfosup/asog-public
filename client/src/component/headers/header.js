import React from 'react';

class Header extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            searchField : "",
            ...props
        }
    }
}

export default Header;