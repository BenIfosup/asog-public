import React from 'react';
import BigNews from "./bigNews";
import TableNews from "./tableNews";

import HOC from '../container/hoc';
import MetaTags from "react-meta-tags";

const home = () => {
    return(
        <HOC>
            <MetaTags>
                <title>Accueil – All Styles of Gaming</title>
                <meta name="description" content="Site d'actualité des jeux-vidéos: gamer un jour, gamer toujours !" />
                <meta name="title" content="Accueil – All Styles of Gaming" />
            </MetaTags>
            <BigNews/>
            <TableNews/>
        </HOC>
    );
};

export default home;