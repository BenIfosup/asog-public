import "./globals";
import React from 'react';
import 'react-trumbowyg/dist/trumbowyg.min.css'
import Trumbowyg from 'react-trumbowyg';


import 'trumbowyg/dist/langs/fr.min';
import './plugins/trumbowyg.upload';


import 'trumbowyg/dist/plugins/pasteembed/trumbowyg.pasteembed.js';
import 'trumbowyg/dist/plugins/history/trumbowyg.history.js';


class textEditorV2 extends React.Component{

    render(){
        return(
            <div>
                <Trumbowyg id='react-trumbowyg'
                           lang='fr'
                           buttons={
                               [
                                   ['historyUndo','historyRedo'],
                                   ['viewHTML'],
                                   ['formatting'],
                                   'btnGrp-semantic',
                                   ['link'],
                                   ['insertImage', 'upload'],
                                   'btnGrp-justify',
                                   'btnGrp-lists',
                                   ['table'],
                                   ['fullscreen']
                               ]
                           }
                           data={this.props.data}
                           placeholder='Type your text!'
                           onChange={(d) => this.props.onChange(d.currentTarget.innerHTML)}
                           ref="trumbowyg"
                />
            </div>
        );
    }
}

export default textEditorV2;
