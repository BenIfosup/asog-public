import React from 'react';
import HOC from '../container/hoc';
import {Header, Button, Transition, Message} from "semantic-ui-react";
import TextEditor from './textEditorV2'
import axios from 'axios';

class PageEditor extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            title: props.args,
            text: "",
            initialText: "",
            showSuccessMessage: false,
            successMessage: "",
            showErrorMessage: false,
            errorMessage: ""
        }
    }

    componentDidMount() {
        this.getPage().then((val) => {
            this.setState({text: val.data.data.page.text, initialText: val.data.data.page.text})
        }, () => {
            this.showErrorMessage("La page n'a pas être chargée suite à un problème serveur.")
        });
    }

    componentDidUpdate(prevProps, prevState) {

        if(this.props !== prevProps){
            this.setState({
                title: this.props.args
            }, () => {
                this.getPage().then((val) => {
                    this.setState({text: val.data.data.page.text, initialText: val.data.data.page.text});
                }, () => {
                    this.showErrorMessage("La page n'a pas être chargée suite à un problème serveur.")
                });
            });
        }
    }

    getPage(){
        const options = {
            url: "/page",
            method: "POST",
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query:
                    `query{
                        page(title: "${encodeURI(this.state.title)}"){
                            text
                        }
                    }`
                ,
                variable: null
            })
        };
        return axios(options);
    }

    updatePage(){
        const options = {
            url: "/page",
            method: "POST",
            withCredentials: true,
            headers: {'content-type': 'application/json'},
            data: JSON.stringify({
                query:
                    `mutation{
                        editPage(title: "${encodeURI(this.state.title)}", text: "${encodeURI(this.state.text)}")
                    }`
                ,
                variable: null
            })
        };
        axios(options).then(() => {
            this.showSuccessMessage("La page a été éditée avec succès !")
        }, () => {
            this.showErrorMessage("La page n'a pas pu être éditée à cause d'un problème serveur.")
        })
    }

    showSuccessMessage(message){
        this.setState({showSuccessMessage: true, successMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showSuccessMessage: false})
            }, 3000);
        });
    }

    showErrorMessage(message){
        this.setState({showErrorMessage: true, errorMessage: message}, () =>{
            setTimeout(() => {
                this.setState({showErrorMessage: false})
            }, 5000);
        });
    }

    render(){
        return(
            <HOC>
                <Header as="h1">Edition de la page "{this.props.args}"</Header>
                <TextEditor
                    data={this.state.initialText}
                    onChange={(text) => {this.setState({text})}}
                />
                <Button content="Editer" onClick={() => this.updatePage()}/>
                <Transition visible={this.state.showSuccessMessage} animation='fade'>
                    <Message
                        success
                        header='Ok !'
                        content={this.state.successMessage}
                    />
                </Transition>
                <Transition visible={this.state.showErrorMessage} animation='fade'>
                    <Message
                        error
                        header='Problème !'
                        content={this.state.errorMessage}
                    />
                </Transition>
            </HOC>
        );
    }
}

export default PageEditor;