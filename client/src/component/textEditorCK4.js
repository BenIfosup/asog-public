import React from 'react';
import CKEditor from 'ckeditor4-react'

CKEditor.editorUrl = 'https://cdn.ckeditor.com/4.4.5.1/full-all/ckeditor.js';

const textEditorCK4 = (props) => {
    return (
            <CKEditor
                data="<p>Hello from CKEditor 4!</p>"
                onChange={(e)  => props.onChange(e.editor.getData())}
            />
    );
};

export default textEditorCK4;