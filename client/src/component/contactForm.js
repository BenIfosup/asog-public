import React from 'react';
import {Form, Header, Input, Message, TextArea, Transition} from "semantic-ui-react";
import HOC from '../container/hoc';
import MetaTags from "react-meta-tags";
import axios from 'axios';
class ContactForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            subject: "",
            content: "",
            showSuccessMessage: false,
            showErrorMessage: false
        }
    }

    handleSubmit(){

        let data = {
            email: this.state.email,
            subject: this.state.subject,
            content: this.state.content
        };

        const options = {
            data,
            url: "/contact",
            method: "POST"
        };

        axios(options).then(val => {
            if(val.status === 200){
                this.showSuccessMessage();
            }
            else{
                this.showErrorMessage();
            }
        }, () => {
            this.showErrorMessage();
        })
    }

    showSuccessMessage(){
        this.setState({showSuccessMessage: true}, () =>{
            setTimeout(() => {
                this.setState({showSuccessMessage: false})
            }, 3000);
        });
    }

    showErrorMessage(){
        this.setState({showErrorMessage: true}, () =>{
            setTimeout(() => {
                this.setState({showErrorMessage: false})
            }, 5000);
        });
    }

    render() {
        return(
            <HOC>
                <MetaTags>
                    <title>Contactez-nous</title>
                    <meta name="description" content="Formulaire de contact de All Styles Of Gaming" />
                    <meta name="title" content="Formulaire de contact" />
                </MetaTags>
                <Header as={"h1"}>Formulaire de contact </Header>
                <Form onSubmit={() => this.handleSubmit()}>
                    <Form.Field
                        control={Input}
                        id={"email_contact"}
                        label={"Votre adresse email:"}
                        placeholder={"email"}
                        type={"email"}
                        onChange={(e) => this.setState({email: e.target.value})}
                        required
                    />
                    <Form.Field
                        control={Input}
                        id={"subject_email"}
                        label={"Sujet de votre mail:"}
                        placeholder={"Sujet du mail"}
                        type={"text"}
                        onChange={(e) => this.setState({subject: e.target.value})}
                        required
                    />
                    <Form.Field
                        control={TextArea}
                        id={"content_email"}
                        label={"Contenu de votre message"}
                        placeholder={"Votre message..."}
                        onChange={(e) => this.setState({content: e.target.value})}
                        required
                    />
                    <Form.Button>Envoyer</Form.Button>
                    <Transition visible={this.state.showSuccessMessage} animation='fade'>
                        <Message
                            success
                            header='Ok !'
                            content="Votre message a bien été envoyé !"
                        />
                    </Transition>
                    <Transition visible={this.state.showErrorMessage} animation='fade'>
                        <Message
                            error
                            header='Problème !'
                            content={"Nous sommes désolés, mais votre message n'a pas pu être envoyé. Faites une sauvegarde de celui-ci et réessayer plus tard."}
                        />
                    </Transition>
                </Form>
            </HOC>
        )
    }
}

export default ContactForm;