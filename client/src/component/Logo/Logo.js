import React from 'react';
import logoUrl from './logo2.svg'

const logo = (props) => {
    return (
        <object className={"logo"} data={logoUrl} aria-label="Logo du site">{props.alternateText}</object>
    )
};

export default logo;