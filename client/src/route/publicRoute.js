import React, { Component, Suspense} from 'react';
import Footer from '../component/footer';
import {Switch, Route} from "react-router-dom";
import PublicContainer from '../container/PublicContainer';
import withSizes from 'react-sizes'

const HeaderMobile = React.lazy(() => import('../component/headers/headerMobile'));
const HeaderDesktop = React.lazy(() => import('../component/headers/headerDesktop'));


class PublicRoute extends Component{

    render() {
        return(
            <Suspense fallback={<div>Loading...</div>}>
                {
                    this.props.isMobile ?
                        (
                            <HeaderMobile>
                                <Switch>
                                    <Route path={'/article/:title'} exact
                                           render={(props) => <PublicContainer {...props} content={"article"}/>}/>
                                    <Route path={'/generalSearch/:searchField'} exact
                                           render={(props) => <PublicContainer {...props} content={"search"}/>}/>
                                    <Route path={'/search/:categorie'} exact
                                           render={(props) => <PublicContainer {...props} content={"search"}/>}/>
                                    <Route path={'/page/:title'} exact
                                           render={(props) => <PublicContainer {...props} content={"page"}/>}/>
                                    <Route path={'/contact/'} exact
                                           render={(props) => <PublicContainer {...props} content={"contact"}/>}/>
                                    <Route path={'/'} render={() => <PublicContainer content={"home"}/>}/>
                                </Switch>
                                <Footer/>
                            </HeaderMobile>
                        )
                        :
                        (
                            <div className={"wrapper"}>
                                <HeaderDesktop/>
                                <Switch>
                                    <Route path={'/article/:title'} exact
                                           render={(props) => <PublicContainer {...props} content={"article"}/>}/>
                                    <Route path={'/generalSearch/:searchField'} exact
                                           render={(props) => <PublicContainer {...props} content={"search"}/>}/>
                                    <Route path={'/search/:categorie'} exact
                                           render={(props) => <PublicContainer {...props} content={"search"}/>}/>
                                    <Route path={'/page/:title'} exact
                                           render={(props) => <PublicContainer {...props} content={"page"}/>}/>
                                    <Route path={'/contact/'} exact
                                           render={(props) => <PublicContainer {...props} content={"contact"}/>}/>
                                    <Route path={'/'} render={() => <PublicContainer content={"home"}/>}/>
                                </Switch>
                                <Footer/>
                            </div>
                        )
                }
            </Suspense>
        );
    }
}

const mapSizesToProps = ({ width }) => ({
    isMobile: width < 768,
});

export default withSizes(mapSizesToProps)(PublicRoute);