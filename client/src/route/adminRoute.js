import React, { Component} from 'react';
import HOC from '../container/hoc';
import {connect} from "react-redux";
import {Switch, Route, Redirect} from "react-router-dom";
import MyContainer from '../container/MyContainer';
import ConnexionForm from '../component/connexionForm';

class AdminRoute extends Component{
    render(){
        return(
            <HOC>
                {
                    !this.props.connected ?
                        (<Switch>
                            <Route path={"/admin/connexion"} exact component={ConnexionForm}/>
                            <Route path={"/admin/"} render={() => <Redirect to={"/admin/connexion"}/>}/>
                        </Switch>) :
                        (
                            <Switch>
                                <Route path={"/admin/addArticle"} exact
                                       render={() => <MyContainer right="ArticleForm"/>}/>
                                <Route path={"/admin/edit/:id"} exact
                                       render={(props) => <MyContainer {...props} right="ArticleForm"/>}/>
                                <Route path={"/admin/gestion/pages/:title"} exact
                                       render={(props) => <MyContainer {...props} right="PageEditor"/>}/>
                                <Route path={"/admin/gestion/:categorie"} exact
                                       render={(props) => <MyContainer {...props} right="SearchAdmin"/>}/>
                                <Route path={"/admin/"} render={() => <Redirect to={"/admin/addArticle"}/>}/>
                            </Switch>
                        )
                }
            </HOC>
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        connected : state.connexion.connected
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        enableConnexion: (id) => {
            dispatch({type: "enableConnexion", payload:{id}});
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminRoute);