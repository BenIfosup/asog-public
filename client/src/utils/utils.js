class Utils{

    static getGoodImageSize(){
        const devicesWidth = [500, 1280, 1920];
        let nbrString = window.innerWidth;
        if(isNaN(nbrString)){
            return devicesWidth[0];
        }
        const widthNumber = parseInt(nbrString);
        for(let i = ((devicesWidth.length)-1); i >= 0; i--){
            if(devicesWidth[i] <= widthNumber){
                return devicesWidth[i];
            }
        }
        return devicesWidth[0];
    }

    static imageURLGoodDimension(string){
        return string.replace(/(https:\/\/www\.allstylesofgaming.com\/upload\/images\/(?:(\S)*)")/gm,
            function(match) {
               return match.substr(0, match.length-1)+'/'+Utils.getGoodImageSize()
        });
    }
}

export default Utils;