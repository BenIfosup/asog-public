const imageManager = require('../model/imagesManager');
const uuid = require("uuid/v4");

exports.uploadImage = (req, res) => {
    if(req.session.connected){
        imageManager.createPictures(req.file.buffer, uuid()).then(path => {
            res.json({success: true, url: "https://www.allstylesofgaming.com"+ path.substr(1)});
        })
    }
    else{
        res.sendStatus(401);
    }
};