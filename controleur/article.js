const db = require('../model/DB/db');
const ArticleDB = db.Article;
const UserDB = db.User;
const graphql = require("graphql");
const graphql_iso_date = require("graphql-iso-date");
const sanitize = require('mongo-sanitize');
const imageManager = require('../model/imagesManager');
const uuid = require("uuid/v4");

const devicesWidth = [500, 720, 1980];
function _findGoodWidth(nbrString){
    if(isNaN(nbrString)){
        return devicesWidth[0];
    }
    const widthNumber = parseInt(nbrString);
    for(let i = devicesWidth.length-1; i <= 0; i--){
        if(devicesWidth[i] < widthNumber){
            return devicesWidth[i];
        }
    }
    return devicesWidth[0];
}

const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull,
    GraphQLBoolean
} = graphql;

const {
    GraphQLDate,
    GraphQLTime,
    GraphQLDateTime
} = graphql_iso_date;


const ArticleType = new GraphQLObjectType({
    name:"Article",
    fields: () => ({
        id: {type: GraphQLString},
        title: {type: GraphQLString},
        subtitle: {type: GraphQLString},
        categorie: {type: GraphQLString},
        text: {type: GraphQLString},
        date: {type: GraphQLDateTime},
        header: {type: GraphQLString}
    })
});

const UserType = new GraphQLObjectType({
    name: "User",
    fields: () => ({
        id: {type: GraphQLString},
        pseudo: {type: GraphQLString}
    })
});

const ArticleWithAuthorType = new GraphQLObjectType({
    name:"ArticleWithAuthor",
    fields: () => ({
        article: {type: ArticleType},
        author: {type: UserType}
    })
});

const MutationQuery = new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
        addArticle:{
            type: GraphQLBoolean,
            args:{
                title: {type: new GraphQLNonNull(GraphQLString)},
                subtitle: {type: new GraphQLNonNull(GraphQLString)},
                categorie: {type: new GraphQLNonNull(GraphQLString)},
                text: {type: new GraphQLNonNull(GraphQLString)},
                author: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args, context){
                return new Promise((resolve, reject) => {
                    if(!context.request.session.connected){
                        return reject("You must be connected");
                    }
                    if(!context.request.file){
                        return reject("Header for an article must be provided");
                    }
                    else{
                        args = sanitize(args);
                        Object.keys(args).map(k => {
                            args[k] = decodeURI(args[k]);
                        });
                        args.title = args.title.trim();
                        let filename = uuid();
                        let promiseImagesCreation = imageManager.createPictures(context.request.file.buffer, filename);
                        promiseImagesCreation.then( filePath => {
                            let article = new ArticleDB({
                                ...args,
                                header: filePath.substring(1)
                            });
                            article.save().then(() => resolve(true), err => {
                                imageManager.deleteImage(filename+'.jpeg');
                                reject(err);
                            });
                        }, err =>{
                            reject(err);
                        });
                    }

                });
            }
        },
        editArticle:{
            type: GraphQLBoolean,
            args:{
                id: {type: new GraphQLNonNull(GraphQLString)},
                title: {type: GraphQLString},
                subtitle: {type: GraphQLString},
                categorie: {type: GraphQLString},
                text: {type: GraphQLString}
            },
            resolve(parent, args, context){
                let id = args.id;
                delete args.id;
                args = sanitize(args);
                Object.keys(args).map(k => {
                    args[k] = decodeURI(args[k]);
                });
                return new Promise((resolve, reject) => {
                    if(!context.request.session.connected){
                        return reject("You must be connected");
                    }
                    if(context.request.file){
                        let filename = uuid();
                        let promiseImagesCreation = imageManager.createPictures(context.request.file.buffer, filename);
                        let promiseArticle = ArticleDB.findById(id).exec();
                        Promise.all([promiseImagesCreation, promiseArticle]).then(values => {
                            const oldHeaderPath = values[1].header;
                            ArticleDB.findByIdAndUpdate(id, {
                                ...args,
                                header: values[0].substring(1)
                            }).exec().then(() => {
                                imageManager.deleteImage(oldHeaderPath);
                                resolve(true);
                            }, err => {
                                return reject(err)
                            });
                        })
                    }
                    else{
                        let promise = ArticleDB.findByIdAndUpdate(id, {...args}).exec();
                        promise.then((val) => {
                            if(val){
                                resolve(true);
                            }
                            else{
                                resolve(false);
                            }
                        }, err => {
                            reject(err);
                        })
                    }
                })
            }
        },
        deleteArticle:{
            type: GraphQLBoolean,
            args: {
                id: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parent, args, context){
                args = sanitize(args);
                return new Promise((resolve, reject) => {
                    if(!context.request.session.connected){
                        return reject("You must be connected");
                    }
                    ArticleDB.findByIdAndRemove(args.id).exec().then((article) => {
                        imageManager.deleteImage(article.header.split("/").pop());
                        resolve(true);
                    }, () => {
                        reject(false);
                    });
                });
            }
        }
    })
});

const QueryRoot = new GraphQLObjectType({
    name: "QueryRoot",
    fields: () => ({
        articles:{
            type: new GraphQLNonNull(GraphQLList(ArticleType)),
            args: {
                ids: {type: new GraphQLNonNull(GraphQLList(new GraphQLNonNull(GraphQLString)))}
            },
            resolve(parent, args){
                args = sanitize(args);
                let reponseP = args.ids.map(id => {
                    return ArticleDB.findById(id);
                });
                return Promise.all(reponseP);
            }
        },
        lastArticles:{
            type: new GraphQLNonNull(GraphQLList(ArticleType)),
            args:{
                nbr: {type: GraphQLInt}
            },
            resolve(parent, args, context) {
                args = sanitize(args);
                return new Promise ((resolve, reject) => {
                    let promise = null;
                    if(args.nbr){
                        promise = ArticleDB.find({}).sort({date: 'desc', _id: 'desc'}).limit(args.nbr).exec();
                    }
                    else{
                        promise = ArticleDB.find({}).sort({date: 'desc', _id:'desc'}).limit(10).exec();
                    }
                    promise.then(articles => {
                        resolve(articles);
                    }, err => reject(err));
                });
            }
        },
        filteredArticles: {
            type: new GraphQLNonNull(GraphQLList(ArticleType)),
            args:{
                categorie: {type: GraphQLString}
            },
            resolve(parents, args){
                args = sanitize(args);
                args.categorie = decodeURI(args.categorie);
                if(args.categorie){
                    return ArticleDB.find({categorie: args.categorie}).sort({date: 'desc', _id: 'desc'}).exec();
                }
                else{
                    return ArticleDB.find({}).sort({date: 'desc', _id: 'desc'}).exec();
                }
            }
        },
        filteredArticlesByString: {
            type: new GraphQLNonNull(GraphQLList(ArticleType)),
            args:{
                string: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parents, args){
                args = sanitize(args);
                args.string = decodeURI(args.string);
                return ArticleDB
                    .find()
                    .or([
                        {titre: {$regex: args.string, $options: 'i'}},
                        {categorie: {$regex: args.string, $options: 'i'}}
                    ])
                    .sort({date: 'desc', _id: 'desc'}).exec();
            }
        },
        articleByTitle:{
            type: new GraphQLNonNull(ArticleType),
            args:{
                title: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parents, args){
                args = sanitize(args);
                args.title = decodeURI(args.title);
                return ArticleDB.findOne({title: args.title}).exec();
            }
        },
        articleWithAuthorByTitle: {
            type: ArticleWithAuthorType,
            args:{
                title: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parents, args, context){
                return new Promise((resolve, reject) => {
                    args = sanitize(args);
                    args.title = decodeURI(args.title);
                    console.log(args.title);
                    ArticleDB.findOne({title: args.title}).then(article => {
                        if(article){
                            UserDB.findById(article.author).then(author => {
                                resolve({
                                    article,
                                    author
                                });
                            }, err => {
                                console.log(err);
                                reject(err);
                            });
                        }
                        else{
                            reject("Article not found");
                        }
                    }, err => {
                        console.log(err);
                        reject(err);
                    });
                });
            }
        }
    })
});

module.exports = new GraphQLSchema({
    query: QueryRoot,
    mutation: MutationQuery
});